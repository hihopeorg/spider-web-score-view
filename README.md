# SpiderWebScoreView

**本项目是基于开源项目SpiderWebScoreView进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/panpf/spider-web-score-view ）追踪到原项目版本**

#### 项目介绍

- 项目名称：蛛网评分控件

- 所属系列：ohos的第三方组件适配移植

- 功能：

  1.显示蛛网图形
  
  2.显示蜘蛛网图形四周的文案

- 项目移植状态：完成

- 调用差异：无

- 项目作者和维护人：hihope

- 联系方式：hihope@hoperun.com

- 原项目Doc地址：https://github.com/panpf/spider-web-score-view

- 原项目基线版本：v1.0.1,sha1:2d3df5308a046c56904b220886d719d615f1b3d2

- 编程语言：Java

#### 效果展示
<img src="screenshot/效果展示.jpg"/>

#### 安装教程

方案一：

1. 添加har包到lib文件夹内。

2. 在entry的build.gradle内添加如下代码。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
	……
}
```

方案二：

1.在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址:

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2.在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'me.panpf.swsv.sample.ohos:SpiderWebScoreView:1.0.0'
}
```

#### 使用说明

1. 布局文件中使用

   ```
   <StackLayout
       ohos:height="150vp"
       ohos:width="match_parent">

       <me.panpf.swsv.SpiderWebScoreView
           ohos:id="$+id:spiderWeb_mainAbility_1"
           ohos:height="100vp"
           ohos:width="100vp"
           ohos:layout_alignment="center"
           app:angleCount="3"
           app:hierarchyCount="3"/>

       <me.panpf.swsv.CircularLayout
           ohos:id="$+id:layout_mainAbility_circular1"
           ohos:height="125vp"
           ohos:width="125vp"
           ohos:layout_alignment="center" />
   </StackLayout>
   ```

2. 代码中使用
   
   ```
    @Override
        public void onStart(Intent intent) {
            super.onStart(intent);
            super.setUIContent(ResourceTable.Layout_ability_main);
            SpiderWebScoreView spiderWebScoreView1 = (SpiderWebScoreView) findComponentById(ResourceTable.Id_spiderWeb_mainAbility_1);
    
            CircularLayout circularLayout1 = (CircularLayout) findComponentById(ResourceTable.Id_layout_mainAbility_circular1);
    
            setup(spiderWebScoreView1, circularLayout1, new Score(7.0f), new Score(8.0f), new Score(5.0f));
        }
    
        private void setup(SpiderWebScoreView spiderWebScoreView, CircularLayout circularLayout, Score... scores) {
            spiderWebScoreView.setScores(10f, assembleScoreArray(scores));
    
            circularLayout.removeAllComponents();
            for (int i = 0; i < scores.length; i++) {
                Text scoreTextView = (Text) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_score, circularLayout, false);
                scoreTextView.setText(String.valueOf(scores[i].score));
                if (scores[i].iconId != 0) {
                    try {
                        Resource resource = getResourceManager().getResource(scores[i].iconId);
                        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                        decodingOptions.desiredSize = new Size(20, 20);
                        PixelMap pixelmap = ImageSource.create(resource, new ImageSource.SourceOptions()).createPixelmap(decodingOptions);
                        PixelMapElement element = new PixelMapElement(pixelmap);
                        scoreTextView.setAroundElements(null, null, element, null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (NotExistException e) {
                        e.printStackTrace();
                    }
                }
                circularLayout.addComponent(scoreTextView, i, new ComponentContainer.LayoutConfig());
            }
        }
    
        private float[] assembleScoreArray(Score... scores) {
            float[] scoreArray = new float[scores.length];
            for (int w = 0; w < scores.length; w++) {
                scoreArray[w] = scores[w].score;
            }
            return scoreArray;
        }
    
        private static class Score {
            private float score;
            private int iconId;
    
            private Score(float score, int iconId) {
                this.score = score;
                this.iconId = iconId;
            }
    
            private Score(float score) {
                this.score = score;
            }
        }
    
   ```
3. 属性说明

    |属性名|介绍|对应方法|缺省值|
    |:--|:--|:--|:--|
    |angleCount|设置蛛网图形有多少个角|会在setScores(float, float[])方法中根据scores的长度来覆盖此参数|5|
    |hierarchyCount|设置蛛网图形有多少层|setHierarchyCount(int)|5|
    |maxScore|最大分值|setScores(float, float[])方法的第一个参数就是maxScore|10f|
    |lineColor|蛛网线条的颜色|setLineColor(int)|0xFF000000|
    |lineWidth|蛛网线条的宽度|setLineWidth(float)|-1(不设置，Paint默认宽度)|
    |scoreColor|分数图形的颜色|setScoreColor(int)|0x80F65801|
    |scoreStrokeColor|分数图形描边的颜色|setScoreStrokeColor(int)|0xFFF65801|
    |scoreStrokeWidth|分数图形描边的宽度|setScoreStrokeWidth(float)|-1(不设置，Paint默认宽度)|
    |disableScoreStroke|禁用分数图形描边|setDisableScoreStroke(boolean)|false|

#### 版本迭代

- v1.0.0

实现功能

- 蜘蛛网图形 
- 图形四周的文案


#### 版权和许可信息

Apache License, Version 2.0 
