package me.panpf.swsv.sample.unit;

import me.panpf.swsv.CircularLayout;
import me.panpf.swsv.SpiderWebScoreView;
import me.panpf.swsv.sample.slice.MainAbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Optional;

import static junit.framework.TestCase.*;

public class SpiderWebScoreTest {
    private static IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;
    private AttrSet attrSet;

    @Before
    public void setUp() {
        mContext = abilityDelegator.getAppContext();
        attrSet = new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        };
    }

    @Test
    public void setScores() throws Exception {
        SpiderWebScoreView scoreView = new SpiderWebScoreView(mContext, attrSet);
        try {
            scoreView.setScores(10, new float[]{});
        } catch (Exception e) {
            assertTrue(true);
        }
        scoreView.setScores(100f, new float[]{5.0f, 6.0f, 7.0f, 8.0f});
        Field field = SpiderWebScoreView.class.getDeclaredField("maxScore");
        field.setAccessible(true);
        float maxScore = (float) field.get(scoreView);
        assertEquals(100f, maxScore);
    }

    @Test
    public void setHierarchyCount() throws Exception {
        SpiderWebScoreView scoreView = new SpiderWebScoreView(mContext, attrSet);
        try {
            scoreView.setHierarchyCount(0);
        } catch (Exception e) {
            assertTrue(true);
        }
        scoreView.setHierarchyCount(4);
        Field field = SpiderWebScoreView.class.getDeclaredField("hierarchyCount");
        field.setAccessible(true);
        int hierarchyCount = (int) field.get(scoreView);
        assertEquals(4, hierarchyCount);
    }

    @Test
    public void setLineColor() throws Exception {
        SpiderWebScoreView scoreView = new SpiderWebScoreView(mContext, attrSet);
        scoreView.setLineColor(Color.BLACK.getValue());
        Field field = SpiderWebScoreView.class.getDeclaredField("linePaint");
        field.setAccessible(true);
        Paint linePaint = (Paint) field.get(scoreView);
        assertEquals(Color.BLACK.getValue(), linePaint.getColor().getValue());
    }

    @Test
    public void setLineWidth() throws Exception {
        SpiderWebScoreView scoreView = new SpiderWebScoreView(mContext, attrSet);
        scoreView.setLineWidth(5f);
        Field field = SpiderWebScoreView.class.getDeclaredField("linePaint");
        field.setAccessible(true);
        Paint linePaint = (Paint) field.get(scoreView);
        assertEquals(5f, linePaint.getStrokeWidth());
    }

    @Test
    public void setScoreColor() throws Exception {
        SpiderWebScoreView scoreView = new SpiderWebScoreView(mContext, attrSet);
        scoreView.setScoreColor(Color.BLACK.getValue());
        Field field = SpiderWebScoreView.class.getDeclaredField("scorePaint");
        field.setAccessible(true);
        Paint scorePaint = (Paint) field.get(scoreView);
        assertEquals(Color.BLACK.getValue(), scorePaint.getColor().getValue());
    }

    @Test
    public void setScoreStrokeColor() throws Exception {
        SpiderWebScoreView scoreView = new SpiderWebScoreView(mContext, attrSet);
        scoreView.setScoreStrokeColor(Color.BLACK.getValue());
        Field field = SpiderWebScoreView.class.getDeclaredField("scoreStrokePaint");
        field.setAccessible(true);
        Paint scoreStrokePaint = (Paint) field.get(scoreView);
        assertNull(scoreStrokePaint);
    }

    @Test
    public void setScoreStrokeWidth() throws Exception {
        SpiderWebScoreView scoreView = new SpiderWebScoreView(mContext, attrSet);
        scoreView.setScoreStrokeWidth(5f);
        Field field = SpiderWebScoreView.class.getDeclaredField("scoreStrokePaint");
        field.setAccessible(true);
        Paint scoreStrokePaint = (Paint) field.get(scoreView);
        assertNull(scoreStrokePaint);
    }
}
