package me.panpf.swsv.sample.unit;

import me.panpf.swsv.CircularLayout;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.app.Context;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;

public class CircularLayoutTest {
    private static IAbilityDelegator abilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;
    private AttrSet attrSet;

    @Before
    public void setUp() {
        mContext = abilityDelegator.getAppContext();
        attrSet = new AttrSet() {
            @Override
            public Optional<String> getStyle() {
                return Optional.empty();
            }

            @Override
            public int getLength() {
                return 0;
            }

            @Override
            public Optional<Attr> getAttr(int i) {
                return Optional.empty();
            }

            @Override
            public Optional<Attr> getAttr(String s) {
                return Optional.empty();
            }
        };
    }

    @Test
    public void addComponent() {
        CircularLayout circularLayout = new CircularLayout(mContext, attrSet);
        assertEquals(0, circularLayout.getChildCount());
        circularLayout.addComponent(new Text(mContext), 0, new ComponentContainer.LayoutConfig());
        assertEquals(1, circularLayout.getChildCount());
    }
}