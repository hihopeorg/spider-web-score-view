package me.panpf.swsv.sample.slice;

import me.panpf.swsv.CircularLayout;
import me.panpf.swsv.SpiderWebScoreView;
import me.panpf.swsv.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.io.IOException;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        SpiderWebScoreView spiderWebScoreView1 = (SpiderWebScoreView) findComponentById(ResourceTable.Id_spiderWeb_mainAbility_1);
        SpiderWebScoreView spiderWebScoreView2 = (SpiderWebScoreView) findComponentById(ResourceTable.Id_spiderWeb_mainAbility_2);
        SpiderWebScoreView spiderWebScoreView3 = (SpiderWebScoreView) findComponentById(ResourceTable.Id_spiderWeb_mainAbility_3);
        SpiderWebScoreView spiderWebScoreView4 = (SpiderWebScoreView) findComponentById(ResourceTable.Id_spiderWeb_mainAbility_4);
        SpiderWebScoreView spiderWebScoreView5 = (SpiderWebScoreView) findComponentById(ResourceTable.Id_spiderWeb_mainAbility_5);

        CircularLayout circularLayout1 = (CircularLayout) findComponentById(ResourceTable.Id_layout_mainAbility_circular1);
        CircularLayout circularLayout2 = (CircularLayout) findComponentById(ResourceTable.Id_layout_mainAbility_circular2);
        CircularLayout circularLayout3 = (CircularLayout) findComponentById(ResourceTable.Id_layout_mainAbility_circular3);
        CircularLayout circularLayout4 = (CircularLayout) findComponentById(ResourceTable.Id_layout_mainAbility_circular4);
        CircularLayout circularLayout5 = (CircularLayout) findComponentById(ResourceTable.Id_layout_mainAbility_circular5);

        setup(spiderWebScoreView1, circularLayout1, new Score(7.0f), new Score(8.0f), new Score(5.0f));
        setup(spiderWebScoreView2, circularLayout2, new Score(7.0f), new Score(8.0f), new Score(5.0f), new Score(5.0f), new Score(8.0f));
        setup(spiderWebScoreView3, circularLayout3, new Score(7.0f), new Score(8.0f), new Score(5.0f), new Score(5.0f), new Score(8.0f), new Score(7.0f), new Score(8.0f), new Score(5.0f));
        setup(spiderWebScoreView4, circularLayout4, new Score(7.0f), new Score(8.0f), new Score(5.0f), new Score(5.0f), new Score(8.0f), new Score(7.0f), new Score(8.0f), new Score(5.0f), new Score(5.0f), new Score(8.0f), new Score(7.0f), new Score(8.0f));
        setup(spiderWebScoreView5, circularLayout5, new Score(7.0f, ResourceTable.Media_vip_icon7), new Score(8.0f, ResourceTable.Media_vip_icon8), new Score(5.0f, ResourceTable.Media_vip_icon5), new Score(5.0f, ResourceTable.Media_vip_icon5), new Score(8.0f, ResourceTable.Media_vip_icon8), new Score(7.0f, ResourceTable.Media_vip_icon7));
    }

    private void setup(SpiderWebScoreView spiderWebScoreView, CircularLayout circularLayout, Score... scores) {
        spiderWebScoreView.setScores(10f, assembleScoreArray(scores));

        circularLayout.removeAllComponents();
        for (int i = 0; i < scores.length; i++) {
            Text scoreTextView = (Text) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_score, circularLayout, false);
            scoreTextView.setText(String.valueOf(scores[i].score));
            if (scores[i].iconId != 0) {
                try {
                    Resource resource = getResourceManager().getResource(scores[i].iconId);
                    ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
                    decodingOptions.desiredSize = new Size(20, 20);
                    PixelMap pixelmap = ImageSource.create(resource, new ImageSource.SourceOptions()).createPixelmap(decodingOptions);
                    PixelMapElement element = new PixelMapElement(pixelmap);
                    scoreTextView.setAroundElements(null, null, element, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            circularLayout.addComponent(scoreTextView, i, new ComponentContainer.LayoutConfig());
        }
    }

    private float[] assembleScoreArray(Score... scores) {
        float[] scoreArray = new float[scores.length];
        for (int w = 0; w < scores.length; w++) {
            scoreArray[w] = scores[w].score;
        }
        return scoreArray;
    }

    private static class Score {
        private float score;
        private int iconId;

        private Score(float score, int iconId) {
            this.score = score;
            this.iconId = iconId;
        }

        private Score(float score) {
            this.score = score;
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
