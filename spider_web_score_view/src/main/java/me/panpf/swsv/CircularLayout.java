/*
 * Copyright (C) 2016 Peng fei Pan <sky@panpf.me>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.panpf.swsv;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
/**
 * 圆形布局，所有子视图按照圆形排列并且在圆圈的外面
 */
public class CircularLayout extends ComponentContainer implements Component.EstimateSizeListener, ComponentContainer.ArrangeListener {

    private static final int LOCATION_EAST = 1;
    private static final int LOCATION_WEST = 2;
    private static final int LOCATION_SOUTH = 3;
    private static final int LOCATION_NORTH = 4;
    private static final int LOCATION_EAST_NORTH = 5;
    private static final int LOCATION_EAST_SOUTH = 6;
    private static final int LOCATION_WEST_NORTH = 7;
    private static final int LOCATION_WEST_SOUTH = 8;

    private int spacing;    // 子View与圆环之间的间距

    private float centerX;    // 中心点X坐标
    private float centerY;    // 中心点Y坐标
    private float radius;   // 半径
    private int childCount; // 子View个数

    public CircularLayout(Context context) {
        this(context, null);
    }

    public CircularLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public CircularLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        setClipEnabled(true);
        if (attrSet != null) {
            parseAttrs(attrSet);
        }
        setEstimateSizeListener(this);
        setArrangeListener(this);
    }

    private void parseAttrs(AttrSet attrSet) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getRealAttributes();
        int defaultSpacing = (int) (attributes.densityPixels * 8 + 0.5f);
        if (attrSet.getAttr("spacing").isPresent()) {
            defaultSpacing = attrSet.getAttr("spacing").get().getDimensionValue();
        }
        setSpacing(defaultSpacing);
    }

    @Override
    public void addComponent(Component childComponent, int index, LayoutConfig layoutConfig) {
        super.addComponent(childComponent, index, layoutConfig);
        childComponent.setClipEnabled(false);
        childCount = getChildCount();
        reset();
    }


    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        setEstimatedSize(widthMeasureSpec, heightMeasureSpec);
        return false;
    }

    private void reset() {
        int viewWidth = getWidth();
        int viewHeight = getHeight();
        centerX = viewWidth >> 1;
        centerY = viewHeight >> 1;
        radius = Math.min(viewWidth, viewHeight) >> 1;
    }

    @Override
    public boolean onArrange(int left, int top, int right, int bottom) {
        if (childCount == 0) {
            return true;
        }
        // 循环处理每个位置的子View，先计算出子View在圆上所处的位置，然后按照其子View的宽高偏移一定的距离，保证子View全部在圆圈之外，并且子View的中心点和其圆上的点连同圆心在一条直线上
        Component childView;
        float nextAngle;
        float nextRadians;
        float nextPointX;
        float nextPointY;
        int childViewMeasuredWidth;
        int childViewMeasuredHeight;
        float childViewLeft;
        float childViewTop;
        float averageAngle = childCount > 0 ? 360 / childCount : 0;
        float offsetAngle = averageAngle > 0 && childCount % 2 == 0 ? averageAngle / 2 : 0;    // 偏移角度，有助于让整个图形左右对称
        for (int position = 0, size = getChildCount(); position < size; position++) {
            childView = getComponentAt(position);
            nextAngle = offsetAngle + (position * averageAngle);
            nextRadians = (float) Math.toRadians(nextAngle);
            nextPointX = (float) (centerX + Math.sin(nextRadians) * radius);
            nextPointY = (float) (centerY - Math.cos(nextRadians) * radius);
            childViewMeasuredWidth = childView.getEstimatedWidth();
            childViewMeasuredHeight = childView.getEstimatedHeight();

            childViewLeft = nextPointX;
            childViewTop = nextPointY;
            switch (calculateLocationByAngle(nextAngle)) {
                case LOCATION_NORTH:
                    childViewLeft -= childViewMeasuredWidth >> 1;
                    childViewTop -= childViewMeasuredHeight;

                    childViewTop += spacing;
                    break;
                case LOCATION_EAST_NORTH:
                    childViewTop -= childViewMeasuredHeight >> 1;

                    childViewLeft -= spacing >> 1;
                    childViewTop += spacing >> 1;
                    break;
                case LOCATION_EAST:
                    childViewTop -= childViewMeasuredHeight >> 1;

                    childViewLeft -= spacing;
                    break;
                case LOCATION_EAST_SOUTH:
                    childViewLeft -= spacing;
                    childViewTop -= spacing;
                    break;
                case LOCATION_SOUTH:
                    childViewLeft -= childViewMeasuredWidth >> 1;

                    childViewTop -= spacing;
                    break;
                case LOCATION_WEST_SOUTH:
                    childViewLeft -= childViewMeasuredWidth;

                    childViewLeft += spacing;
                    childViewTop -= spacing;
                    break;
                case LOCATION_WEST:
                    childViewLeft -= childViewMeasuredWidth;
                    childViewTop -= childViewMeasuredHeight >> 1;

                    childViewLeft += spacing;
                    break;
                case LOCATION_WEST_NORTH:
                    childViewLeft -= childViewMeasuredWidth;
                    childViewTop -= childViewMeasuredHeight >> 1;

                    childViewLeft += spacing >> 1;
                    childViewTop += spacing >> 1;

                    break;
            }
            childView.arrange((int) childViewLeft, (int) childViewTop, childViewMeasuredWidth, childViewMeasuredHeight);
        }
        return false;
    }

    /**
     * 根据角度判断所处的方位，CircularLayout把一个圆分成了8个方位（东、南、西、北、西北、东北、西南、东南），不同的方位有不同的偏移方式
     *
     * @param angle 角度
     * @return 方位
     */
    private int calculateLocationByAngle(float angle) {
        if ((angle >= 337.5f && angle <= 360f) || (angle >= 0f && angle <= 22.5f)) {
            return LOCATION_NORTH;
        } else if (angle >= 22.5f && angle <= 67.5f) {
            return LOCATION_EAST_NORTH;
        } else if (angle >= 67.5f && angle <= 112.5f) {
            return LOCATION_EAST;
        } else if (angle >= 112.5f && angle <= 157.5) {
            return LOCATION_EAST_SOUTH;
        } else if (angle >= 157.5 && angle <= 202.5) {
            return LOCATION_SOUTH;
        } else if (angle >= 202.5 && angle <= 247.5) {
            return LOCATION_WEST_SOUTH;
        } else if (angle >= 247.5 && angle <= 292.5) {
            return LOCATION_WEST;
        } else if (angle >= 292.5 && angle <= 337.5) {
            return LOCATION_WEST_NORTH;
        } else {
            throw new IllegalArgumentException("error angle " + angle);
        }
    }

    /**
     * 设置内容和圆圈之间的距离，默认为8dp
     *
     * @param spacing 内容和圆圈之间的距离
     */
    public void setSpacing(int spacing) {
        this.spacing = spacing;
        postLayout();
    }
}
