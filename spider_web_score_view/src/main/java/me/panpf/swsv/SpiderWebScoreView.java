/*
 * Copyright (C) 2016 Peng fei Pan <sky@panpf.me>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.panpf.swsv;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.app.Context;

import static ohos.agp.components.InputAttribute.PATTERN_TEXT;

/**
 * 蛛网评分图，支持任意条边以及任意层级
 */
public class SpiderWebScoreView extends Component implements Component.DrawTask, Text.EditorActionListener {

    private int angleCount = 5; // 整个蛛网有几个角
    private int hierarchyCount = 5;  // 整个蛛网分多少层（例如最大分数是10分，分5层，那么每层就代表2分）
    private int lineColor = 0xFF000000; // 蛛网线条的颜色
    private float lineWidth = -1; // 蛛网线条的宽度

    private float maxScore = 10f;   // 最大分数
    private float[] scores;  // 分数列表
    private int scoreColor = 0x80F65801; // 分数图形的颜色
    private int scoreStrokeColor = 0xFFF65801; // 分数图形描边的颜色
    private float scoreStrokeWidth = -1; // 分数图形描边的宽度
    private boolean disableScoreStroke; // 禁用分数图形的描边
    private Paint scorePaint;
    private Paint scoreStrokePaint;

    private float centerX;    // 中心点X坐标
    private float centerY;    // 中心点Y坐标
    private float radius; // 整个蛛网图的半径
    private Paint linePaint;
    private Path path;

    public SpiderWebScoreView(Context context) {
        this(context, null);
    }

    public SpiderWebScoreView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public SpiderWebScoreView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrs) {
        setClipEnabled(false);
        if (attrs != null) {
            parseAttrs(attrs);
        }

        linePaint = new Paint();
        linePaint.setColor(new Color(lineColor));
        linePaint.setStyle(Paint.Style.STROKE_STYLE);
        linePaint.setAntiAlias(true);
        if (lineWidth > 0) {
            linePaint.setStrokeWidth(lineWidth);
        }

        scorePaint = new Paint();
        scorePaint.setColor(new Color(scoreColor));
        scorePaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        scorePaint.setAntiAlias(true);

        path = new Path();

        if (onTextEditorAction(PATTERN_TEXT)) {
            float[] randomScoreArray = new float[]{7.0f, 8.0f, 5.0f, 5.0f, 8.0f};
            float[] testScores = new float[angleCount];
            int index = 0;
            for (int w = 0; w < angleCount; w++) {
                testScores[w] = randomScoreArray[index++ % randomScoreArray.length];
            }
            setScores(10f, testScores);
        }
        addDrawTask(this);
    }

    private void parseAttrs(AttrSet attrSet) {
        if (attrSet.getAttr("angleCount").isPresent()) {
            angleCount = attrSet.getAttr("angleCount").get().getIntegerValue();
        }
        if (attrSet.getAttr("hierarchyCount").isPresent()) {
            hierarchyCount = attrSet.getAttr("hierarchyCount").get().getIntegerValue();
        }
        if (attrSet.getAttr("maxScore").isPresent()) {
            maxScore = attrSet.getAttr("maxScore").get().getFloatValue();
        }
        if (attrSet.getAttr("lineColor").isPresent()) {
            lineColor = attrSet.getAttr("lineColor").get().getColorValue().getValue();
        }
        if (attrSet.getAttr("lineWidth").isPresent()) {
            lineWidth = attrSet.getAttr("lineWidth").get().getDimensionValue();
        }
        if (attrSet.getAttr("scoreColor").isPresent()) {
            scoreColor = attrSet.getAttr("scoreColor").get().getColorValue().getValue();
        }
        if (attrSet.getAttr("scoreStrokeColor").isPresent()) {
            scoreStrokeColor = attrSet.getAttr("scoreStrokeColor").get().getColorValue().getValue();
        }
        if (attrSet.getAttr("scoreStrokeWidth").isPresent()) {
            scoreStrokeWidth = attrSet.getAttr("scoreStrokeWidth").get().getFloatValue();
        }
        if (attrSet.getAttr("disableScoreStroke").isPresent()) {
            disableScoreStroke = attrSet.getAttr("disableScoreStroke").get().getBoolValue();
        }
        setAngleCount(angleCount);
        setHierarchyCount(hierarchyCount);
        setMaxScore(maxScore);

        setLineColor(lineColor);
        setLineWidth(lineWidth);
        setScoreColor(scoreColor);
        setScoreStrokeColor(scoreStrokeColor);
        setScoreStrokeWidth(scoreStrokeWidth);
        setDisableScoreStroke(disableScoreStroke);
    }

    private void reset() {
        if (angleCount != 0 && hierarchyCount != 0) {
            int viewWidth = getWidth();
            int viewHeight = getHeight();
            centerX = viewWidth >> 1;
            centerY = viewHeight >> 1;
            radius = Math.min(viewWidth, viewHeight) >> 1;
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        drawAllHierarchy(canvas);
        drawAllLine(canvas);
        drawScore(canvas);
    }

    /**
     * 绘制所有的层
     *
     * @param canvas Canvas
     */
    private void drawAllHierarchy(Canvas canvas) {
        float averageRadius = radius / hierarchyCount;
        for (int w = 0; w < hierarchyCount; w++) {
            drawHierarchyByRadius(canvas, averageRadius * (w + 1));
        }
    }

    /**
     * 根据半径绘制一层
     *
     * @param canvas        Canvas
     * @param currentRadius 当前半径
     */
    private void drawHierarchyByRadius(Canvas canvas, float currentRadius) {
        path.reset();

        float nextAngle;
        float nextRadians;
        float nextPointX;
        float nextPointY;
        float averageAngle = 360 / angleCount;
        float offsetAngle = averageAngle > 0 && angleCount % 2 == 0 ? averageAngle / 2 : 0;
        for (int position = 0; position < angleCount; position++) {
            nextAngle = offsetAngle + (position * averageAngle);
            nextRadians = (float) Math.toRadians(nextAngle);
            nextPointX = (float) (centerX + Math.sin(nextRadians) * currentRadius);
            nextPointY = (float) (centerY - Math.cos(nextRadians) * currentRadius);

            if (position == 0) {
                path.moveTo(nextPointX, nextPointY);
            } else {
                path.lineTo(nextPointX, nextPointY);
            }
        }

        path.close();
        canvas.drawPath(path, linePaint);
    }

    /**
     * 绘制所有的线
     *
     * @param canvas Canvas
     */
    private void drawAllLine(Canvas canvas) {
        float nextAngle;
        float nextRadians;
        float nextPointX;
        float nextPointY;
        float averageAngle = 360 / angleCount;
        float offsetAngle = averageAngle > 0 && angleCount % 2 == 0 ? averageAngle / 2 : 0;
        for (int position = 0; position < angleCount; position++) {
            nextAngle = offsetAngle + (position * averageAngle);
            nextRadians = (float) Math.toRadians(nextAngle);
            nextPointX = (float) (centerX + Math.sin(nextRadians) * radius);
            nextPointY = (float) (centerY - Math.cos(nextRadians) * radius);

            canvas.drawLine(centerX, centerY, nextPointX, nextPointY, linePaint);
        }
    }

    /**
     * 绘制分数图形
     *
     * @param canvas Canvas
     */
    private void drawScore(Canvas canvas) {
        if (scores == null || scores.length <= 0) {
            return;
        }

        path.reset();

        float nextAngle;
        float nextRadians;
        float nextPointX;
        float nextPointY;
        float currentRadius;
        float averageAngle = 360 / angleCount;
        float offsetAngle = averageAngle > 0 && angleCount % 2 == 0 ? averageAngle / 2 : 0;
        for (int position = 0; position < angleCount; position++) {
            currentRadius = (scores[position] / maxScore) * radius;
            nextAngle = offsetAngle + (position * averageAngle);
            nextRadians = (float) Math.toRadians(nextAngle);
            nextPointX = (float) (centerX + Math.sin(nextRadians) * currentRadius);
            nextPointY = (float) (centerY - Math.cos(nextRadians) * currentRadius);

            if (position == 0) {
                path.moveTo(nextPointX, nextPointY);
            } else {
                path.lineTo(nextPointX, nextPointY);
            }
        }

        path.close();
        canvas.drawPath(path, scorePaint);

        // 绘制描边
        if (!disableScoreStroke) {
            if (scoreStrokePaint == null) {
                scoreStrokePaint = new Paint();
                scoreStrokePaint.setColor(new Color(scoreStrokeColor));
                scoreStrokePaint.setStyle(Paint.Style.STROKE_STYLE);
                scoreStrokePaint.setAntiAlias(true);
                if (scoreStrokeWidth > 0) {
                    scoreStrokePaint.setStrokeWidth(scoreStrokeWidth);
                }
            }
            canvas.drawPath(path, scoreStrokePaint);
        }
    }

    /**
     * 设置蛛网有多少个角
     *
     * @param angleCount 蛛网有多少个角
     */
    private void setAngleCount(int angleCount) {
        if (angleCount <= 2) {
            throw new IllegalArgumentException("angleCount Can not be less than or equal to 2");
        }
        this.angleCount = angleCount;
        reset();
        invalidate();
    }

    /**
     * 设置最大分数
     *
     * @param maxScore 最大分数
     */
    private void setMaxScore(float maxScore) {
        if (maxScore <= 0) {
            throw new IllegalArgumentException("maxScore Can not be less than or equal to 0");
        }
        this.maxScore = maxScore;
    }

    /**
     * 设置分数，有多少个人数就有多少个角
     *
     * @param maxScore 最大分数
     * @param scores   分数
     */
    public void setScores(float maxScore, float[] scores) {
        if (scores == null || scores.length == 0) {
            throw new IllegalArgumentException("scores Can't be null or empty");
        }
        setMaxScore(maxScore);
        this.scores = scores;
        this.angleCount = scores.length;
        reset();
        invalidate();
    }

    /**
     * 设置有整个蛛网有多少层
     *
     * @param hierarchyCount 层数
     */
    public void setHierarchyCount(int hierarchyCount) {
        if (hierarchyCount <= 0) {
            throw new IllegalArgumentException("hierarchyCount Can not be less than or equal to 0");
        }
        this.hierarchyCount = hierarchyCount;
        reset();
        invalidate();
    }

    /**
     * 设置蛛网线的颜色
     *
     * @param lineColor 蛛网线的颜色
     */
    public void setLineColor(int lineColor) {
        this.lineColor = lineColor;
        if (linePaint != null) {
            linePaint.setColor(new Color(lineColor));
        }
        invalidate();
    }

    /**
     * 设置蛛网线的宽度
     *
     * @param lineWidth 蛛网线的宽度
     */
    public void setLineWidth(float lineWidth) {
        this.lineWidth = lineWidth;
        if (linePaint != null) {
            linePaint.setStrokeWidth(lineWidth);
        }
        invalidate();
    }

    /**
     * 设置分数图形的颜色
     *
     * @param scoreColor 分数图形的颜色
     */
    public void setScoreColor(int scoreColor) {
        this.scoreColor = scoreColor;
        if (scorePaint != null) {
            scorePaint.setColor(new Color(scoreColor));
        }
        invalidate();
    }

    /**
     * 设置分数图形描边的颜色
     *
     * @param scoreStrokeColor 分数图形描边的颜色
     */
    public void setScoreStrokeColor(int scoreStrokeColor) {
        this.scoreStrokeColor = scoreStrokeColor;
        if (scoreStrokePaint != null) {
            scoreStrokePaint.setColor(new Color(scoreStrokeColor));
        }
        invalidate();
    }

    /**
     * 设置分数图形描边的宽度
     *
     * @param scoreStrokeWidth 分数图形描边的宽度
     */
    public void setScoreStrokeWidth(float scoreStrokeWidth) {
        this.scoreStrokeWidth = scoreStrokeWidth;
        if (scoreStrokePaint != null) {
            scoreStrokePaint.setStrokeWidth(scoreStrokeWidth);
        }
        invalidate();
    }

    /**
     * 设置禁用分数图形的描边
     *
     * @param disableScoreStroke 是否禁用分数图形的描边
     */
    public void setDisableScoreStroke(boolean disableScoreStroke) {
        this.disableScoreStroke = disableScoreStroke;
        invalidate();
    }

    @Override
    public boolean onTextEditorAction(int action) {
        return false;
    }
}
